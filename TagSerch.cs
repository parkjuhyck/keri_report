﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Data.OleDb;
using System.IO;

namespace KERI_Report
{
    public partial class TagSearch : Form
    {
        public int itemCount;
       
        
        Report repo;
        public TagSearch()
        {
            InitializeComponent();
        }

        public TagSearch(Report _form)
        {
            InitializeComponent();
            repo = _form;
        }

        private void TagSerch_Load(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            listView2.Items.Clear();
            listView1.Columns.Add("", 1);
            listView2.Columns.Add("", 1);
            listView1.Columns.Add("TagName", 150, HorizontalAlignment.Center);
            listView1.Columns.Add("Description", 150, HorizontalAlignment.Center);
            listView2.Columns.Add("TagName", 150, HorizontalAlignment.Center);
            listView2.Columns.Add("Description", 150, HorizontalAlignment.Center);
        }

        private void btnSerch_Click(object sender, EventArgs e)
        {
            OleDbConnection conn = new OleDbConnection("Provider=iHistorian OLE DB Provider;;User ID=;Password=;Data Source=;Mode=Read");
            
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = conn;
            conn.Open();
            listView1.Items.Clear();
            cmd.CommandText = "SELECT * FROM ihTags Where tagname = '" + textBox1.Text + "'";
            OleDbDataReader reader1 = cmd.ExecuteReader();
                        
            int j = 1;
            while (reader1.Read())
            {
                ListViewItem lvt = new ListViewItem(j.ToString());
                j = j + 1;
                lvt.SubItems.Add(reader1[0].ToString());
                lvt.SubItems.Add(reader1[1].ToString());
                listView1.Items.Add(lvt);
            }
            
            reader1.Close();
            conn.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            ListViewItem lvt = new ListViewItem();
            lvt.SubItems.Add(listView1.SelectedItems[0].SubItems[1].Text);
            lvt.SubItems.Add(listView1.SelectedItems[0].SubItems[2].Text);
            listView2.Items.Add(lvt);
        }

        private void btnDelete_Click(object sender, EventArgs e) //<<
        {
            if (listView2.SelectedItems.Count == 0)
                MessageBox.Show("태그를 선택하십시오");
            else
                listView2.SelectedItems[0].Remove();
        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            ListViewItem lvt = new ListViewItem();
            lvt.SubItems.Add(listView1.SelectedItems[0].SubItems[1].Text);
            lvt.SubItems.Add(listView1.SelectedItems[0].SubItems[2].Text);
            listView2.Items.Add(lvt);
        }

        private void listView2_DoubleClick(object sender, EventArgs e)
        {
            if (listView2.SelectedIndices.Count > 0)
            {
                listView2.Items.RemoveAt(listView2.SelectedIndices[0]);
            }
        }

        public void btnComplete_Click(object sender, EventArgs e)
        {

            itemCount = listView2.Items.Count;
            Close();
        }

        private void button1_Click(object sender, EventArgs e)//닫기
        {
            Close();
        }
    }
}
